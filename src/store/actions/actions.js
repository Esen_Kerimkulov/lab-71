import axios from '../../axios-dishes';
export const ADD_DISHES = "ADD_DISHES";
export const EDIT_DISHES = "EDIT_DISHES";
export const REMOVE_DISHES = "REMOVE_DISHES";
export const GET_DISHES = "GET_DISHES";

export const addToMenu = () => ({type: ADD_DISHES});
export const getDishesSuccess = (dishes) => ({type: GET_DISHES, dishes});

export const getDishes = () => {
    return (dispatch) => {
        return axios.get('dishes.json').then(response => {
            const dishes = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });

            dispatch(getDishesSuccess(dishes));
        })
    }
};

export const  postRequest = dishes => {
    return dispatch => {
        axios.post('dishes.json', dishes).then(() => {
            dispatch(getDishes())
        })
    }
};



export const deleteDish = (id) => {
    return dispatch => {
        axios.delete('dishes/' + id + '.json').then(() => {
            this.props.history.push('/dishes')
        })
    }
};