import { GET_DISHES } from "./actions/actions";

const initialState = {
    dishes: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_DISHES:
            return {...state, dishes: action.dishes};

        default: return state
    }
};

export default reducer