import React, {Component, Fragment} from 'react';
import axios from 'axios'
import {editPostRequest, postRequest} from "../../store/actions/actions";
import {connect} from "react-redux";

class EditDish extends Component {

    state = {
        name: '',
        price: '',
        imageURL: ''
    };

    componentDidMount() {
        axios.get(`https://laboratory71js.firebaseio.com/dishes/${this.props.match.params.id}.json`).then(response => {
            this.setState({
                name: response.data.name,
                price: response.data.price,
                imageURL: response.data.imageURL
            })
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.state.id !== prevState.id) {
            axios.get(`https://laboratory71js.firebaseio.com/dishes/${this.props.match.params.id}.json`).then(response => {
                this.setState({
                    name: response.data.name,
                    price: response.data.price,
                    imageURL: response.data.imageURL
                })
            })
        }
    }

    changeHandler = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    editPage = (event) => {
        event.preventDefault();
        const data = {
            name: this.state.name,
            price: this.state.price,
            imageURL: this.state.imageURL
        };
        axios.put(`https://laboratory71js.firebaseio.com/dishes/${this.props.match.params.id}.json`, data).then(() => {
            this.props.history.replace('/dishes');
        });
    };

    dishesHandler = () => {
        const dishes = {...this.state};


        this.props.addDishes(dishes);
        this.props.history.push('/dishes')
    };

    render() {
        return (
            <Fragment>
                <h2>Edit Dishes</h2>
                <form onSubmit={this.editPage}>
                    <div>
                        <label htmlFor="Title">Title:</label>
                        <input value={this.state.name} name="name"
                               onChange={this.changeHandler}
                               id="Title" type="text"
                        />
                        <label htmlFor="Image">Image URL:</label>
                        <input value={this.state.imageURL} name="imageURL"
                               onChange={this.changeHandler}
                               id="Image" type="text"
                        />
                        <label htmlFor="Price">Price:</label>
                        <input value={this.state.price} name="price"
                               onChange={this.changeHandler}
                               id="Price" type="text"
                        />
                        <button onClick={this.dishesHandler}>Add To Menu</button>
                    </div>
                </form>
            </Fragment>
        );
    }
}

export default EditDish;