import React, {Component} from 'react';
import {postRequest} from "../../store/actions/actions";
import {connect} from "react-redux";

class AddDish extends Component {
    state = {
        name: '',
        imageURL: '',
        price: '',
    };

    valueChanged = event => {
        const name = event.target.name;
        this.setState({[name]: event.target.value});
    };

    dishesHandler = () => {
        const dishes = {...this.state};

        this.props.addDishes(dishes);
        this.props.history.push('/dishes')
    };

    render() {
        return (
            <div className="AddDishForm">
                <label htmlFor="Title">Title:</label>
                <input value={this.state.name} name="name"
                       onChange={this.valueChanged}
                       id="Title" type="text"
                />
                <label htmlFor="Image">ImageURL:</label>
                <input value={this.state.imageURL} name="imageURL"
                       onChange={this.valueChanged}
                       id="Image" type="text"
                />
                <label htmlFor="Price">Price:</label>
                <input value={this.state.price} name="price"
                       onChange={this.valueChanged}
                       id="Price" type="text"
                />
                <button onClick={this.dishesHandler}>Add To Menu</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loading: state.loading
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addDishes: (dishes) => dispatch(postRequest(dishes))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddDish);