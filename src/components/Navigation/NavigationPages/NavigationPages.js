import React from 'react';
import NavigationPage from "../NavigationPage/NavigationPage";

const NavigationPages = () => {
    return (
        <ul className='NavigationPages'>
            <NavigationPage to='/dishes' exact>Dishes</NavigationPage>
            <NavigationPage to='/orders' exact>Orders</NavigationPage>
        </ul>
    );
};

export default NavigationPages;