import React, {Component} from 'react';
import {connect} from 'react-redux';
import {addNewDish, deleteDish, getDishes, editDish} from '../../store/actions/actions';


class Dishes extends Component {

    componentDidMount() {
        this.props.getDishes().then(() => {
        })
    };

    addNewDish = () => {
        this.props.history.push('/AddDish')
    };

    editDish = (id) => {
        this.props.history.push(`/editDish/${id}`)
    };

    render() {
        return (
            <div className="Dishes">
                <h1>Dishes</h1>
                <div>
                    <button onClick={this.addNewDish}>Add New Dish</button>
                </div>
                {this.props.dishes.map((dish) => {
                    return (
                        <div className="Dish" key={dish.id}>
                            <p>{dish.name}</p>
                            <img alt='image' src={dish.imageURL} />
                            <p>{dish.price}<span>KGS</span></p>
                            <button onClick={() => this.editDish(dish.id)}>edit</button>
                            <button onClick={() => this.props.deleteDish(dish.id)}>delete</button>
                        </div>
                    )
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        dishes: state.dishes
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getDishes: () => dispatch(getDishes()),
        deleteDish: (id) => dispatch(deleteDish(id)),
    }
};



export default connect(mapStateToProps, mapDispatchToProps)(Dishes);