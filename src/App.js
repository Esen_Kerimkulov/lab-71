import React, { Component } from 'react';
import Switch from "react-router-dom/es/Switch";
import Layout from "./components/Layout/Layout";
import {Route} from "react-router-dom";
import Dishes from './components/dishes/dishes';
import './App.css';
import AddDish from "./components/menu/AddDish";
import orders from "./components/orders/orders";
import EditDish from "./components/EditDish/EditDish";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/dishes" component={Dishes}/>
                    <Route path="/addDish" component={AddDish}/>
                    <Route path="/orders" component={orders}/>
                    <Route path="/editDish/:id" component={EditDish}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;