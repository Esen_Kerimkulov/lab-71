import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://laboratory71js.firebaseio.com/'
});

export default instance;